#!/usr/bin/env python3

import requests
import pytz
from datetime import datetime

API_KEY = "0495330ca5d45ca9c3a223901e34de1d"
lat = 45.070312
lon = 7.6868565

def endpoint():
    return f"https://api.openweathermap.org/data/2.5/weather?lang=it&units=metric&lat={lat}&lon={lon}&appid={API_KEY}"

res = requests.get(endpoint())

data = None
print("prepare to write files...")

if res.status_code == 200:
    data = res.json()
    with open("recorder.txt","+a") as f:
        for k,v in data['main'].items():
            f.write(f"{k}: {v}\n")
        f.write(datetime.now(pytz.timezone("Europe/Rome")).__str__())
        f.write("\n\n")
else:
    print(res.json())

print("done!")